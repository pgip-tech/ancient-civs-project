We will work off of the design for Ancient Civilizations in Figma. This fil contains all the colors, fonts, spacing, etc. We will use these to set up the global classes and utility classes. Figma design: https://www.figma.com/file/uHnR2zp1lb2EHg9ukUQXj6/Ancient-Civs?type=design&node-id=0-1&mode=design&t=4FAAoi9lIeCgHZHv-0

Global Styles vs. Utility Classes in CSS

Global styles and utility classes in CSS serve different purposes and have distinct characteristics. Here's a breakdown of their differences:

Scope and Specificity:

Global Styles: Global styles apply to the entire document or website. They often target elements like body, html, or general tags (e.g., a, p, h1) to define default styles for all instances of those elements on the page. Global styles have a low specificity and apply broadly unless overridden.

Utility Classes: Utility classes are narrowly scoped and are designed to apply specific styles to individual HTML elements. They have higher specificity as they target specific elements with class attributes. Utility classes allow for granular control and customization.

Use Case:

Global Styles: Global styles are typically used to establish a consistent base design for your entire website. They define default typography, colors, layout settings, and other foundational styles. They help maintain a cohesive look and feel across the entire site.

Utility Classes: Utility classes are used for quickly adding specific styles or behaviors to elements when needed. They are often employed in component-based development or to address specific, isolated styling requirements. Utility classes are especially useful for rapidly prototyping or making small, one-off style adjustments.

Maintenance:

Global Styles: Global styles can be easier to maintain because they centralize the core styling rules for your site. Changes made to global styles can propagate throughout the entire website, ensuring consistency. However, making changes to global styles requires careful consideration to avoid unintended consequences.

Utility Classes: Utility classes can become challenging to manage if not organized properly. As the number of utility classes increases, it may become harder to keep track of which classes are in use and where. Effective naming conventions and documentation are crucial for maintaining utility classes.

Flexibility:

Global Styles: Global styles provide a consistent starting point for your design but may not be flexible enough to handle every design variation or specific styling need without additional customization.

Utility Classes: Utility classes offer flexibility by allowing you to add or remove specific styles on a per-element basis. They can be combined and customized to achieve a wide range of design variations without affecting other elements.

Performance:

Global Styles: Global styles can potentially result in larger CSS files since they apply to many elements. However, modern CSS minification and compression techniques can mitigate this issue.

Utility Classes: Utility classes can help reduce CSS file size because they only include the styles necessary for specific elements. This can lead to more optimized and smaller CSS files.

In practice, a combination of both global styles and utility classes is often used in web development. Global styles establish a consistent design framework, while utility classes provide the flexibility to make granular style adjustments as needed. The choice between the two depends on the project's requirements and the balance between consistency and customization.


CSS Global Styles:

Global styles in CSS refer to styles that are applied to the entire document or website, affecting all elements unless overridden by more specific styles. These styles are typically defined in a way that they apply universally to the entire web page. Global styles can be used to set default fonts, colors, margins, padding, and other properties that provide a consistent look and feel across your website.

Here are some common ways to implement global styles in CSS:

1. **Using the `*` Selector:**
   You can apply styles to all elements on a web page using the universal selector `*`. For example:

   ```css
   * {
     margin: 0;
     padding: 0;
     box-sizing: border-box;
   }
   ```

   In this example, the `*` selector sets all margins and padding to zero and uses the `border-box` box-sizing model by default for all elements.

2. **Defining Defaults for Typography:**
   You can set default typography styles for your entire website, such as font family, size, line height, and color:

   ```css
   body {
     font-family: Arial, sans-serif;
     font-size: 16px;
     line-height: 1.5;
     color: #333;
   }
   ```

   This style applies to the entire document because it targets the `<body>` element.

3. **Reset or Normalize Styles:**
   Many web developers include CSS resets or normalize.css files at the beginning of their stylesheets to reset or normalize default browser styles, ensuring a consistent baseline across different browsers.

   - CSS Reset example:

     ```css
     /* CSS Reset */
     html, body, div, h1, h2, p {
       margin: 0;
       padding: 0;
       border: 0;
     }
     ```

   - Normalize.css example:

     ```css
     /* Normalize.css */
     /* ... */
     ```

4. **Setting Default Link Styles:**
   You can style links globally to ensure that they have a consistent appearance throughout your site:

   ```css
   a {
     text-decoration: none;
     color: #0077FF;
   }

   a:hover {
     text-decoration: underline;
   }
   ```

   In this example, links are styled to remove underlines and use a specific color by default.

5. **Global Layout Styles:**
   You can also set global layout properties, like the background color or maximum width of the entire page:

   ```css
   body {
     background-color: #f0f0f0;
     max-width: 1200px;
     margin: 0 auto;
   }
   ```

   These styles affect the entire page layout.

Global styles help maintain consistency and simplify the process of maintaining and updating your website's appearance. However, it's essential to use them judiciously and avoid excessive global styles that might make it challenging to override styles for specific elements when needed. It's often a good practice to use a combination of global and specific styles to create a well-organized and maintainable CSS stylesheet.


CSS Utility Classes

Utility classes in CSS are a set of predefined, single-purpose CSS classes that provide specific styling or functionality to HTML elements. These classes are designed to be lightweight and reusable, allowing developers to apply styles or behavior to elements without having to write custom CSS for each use case. Utility classes are particularly popular in modern front-end development frameworks like Bootstrap, Tailwind CSS, and Bulma. Here are some common use cases and examples of utility classes:

1. **Spacing Utilities:**
   Utility classes for controlling margins and padding are common. These classes typically follow a naming convention like `m-{size}` for margins and `p-{size}` for padding, where `{size}` can be a numeric value or a specific size class.

   ```html
   <div class="m-2 p-4">This element has margins and padding applied.</div>
   ```

2. **Text Utilities:**
   Utility classes can control text-related styles like font size, text alignment, and text color.

   ```html
   <p class="text-center text-primary">Centered text in a primary color.</p>
   ```

3. **Background and Color Utilities:**
   Classes for background colors and text colors are handy for quickly applying different color schemes to elements.

   ```html
   <div class="bg-primary text-white">Primary background with white text.</div>
   ```

4. **Display and Flexbox Utilities:**
   Utility classes can control display properties and flexbox behavior.

   ```html
   <div class="d-flex justify-content-between align-items-center">Flex container with alignment.</div>
   ```

5. **Visibility and Hidden Utilities:**
   Classes for hiding or showing elements based on screen size or other conditions.

   ```html
   <div class="d-none d-md-block">Visible on screens with a medium breakpoint or larger.</div>
   ```

6. **Border and Border Radius Utilities:**
   Utility classes for adding borders and controlling border radius.

   ```html
   <div class="border border-primary rounded">A rounded element with a border.</div>
   ```

7. **Sizing Utilities:**
   Classes for controlling element dimensions like width and height.

   ```html
   <div class="w-50 h-100">Half-width and full-height element.</div>
   ```

8. **Responsive Utilities:**
   Classes that apply styles based on screen size or breakpoints.

   ```html
   <div class="text-center text-lg-left">Centered text on small screens and left-aligned on large screens.</div>
   ```

9. **Typography Utilities:**
   Classes for controlling typography styles like font weight and text decoration.

   ```html
   <p class="fw-bold text-underline">Bold text with underline.</p>
   ```

10. **Animation and Transition Utilities:**
    Utility classes for adding animations or transitions to elements.

    ```html
    <button class="btn btn-primary animate-fade-in">Animated button</button>
    ```

These are just a few examples of utility classes, and their exact syntax and naming conventions can vary between different CSS frameworks. Utility classes provide a way to rapidly prototype and style a website without writing custom CSS for each element, making them popular in the development community for their efficiency and consistency. However, it's essential to use them thoughtfully to avoid excessive class proliferation and maintainability challenges.
